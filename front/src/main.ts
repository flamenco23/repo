import { createApp } from 'vue'
import App from './App.vue'
import router from './router'
import io from 'socket.io-client';


createApp(App).use(router).mount('#app')


const socket = io('https://3000-flamenco23-repo-idkfngf8y7y.ws-eu44.gitpod.io/', {
    reconnection: false,
    transports: ["websocket", "polling"]
});
